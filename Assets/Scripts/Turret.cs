using UnityEngine;

public class Turret : MonoBehaviour
{
	private Transform target;

	[Header("Attribute")]
	public float range = 15f;
	//Fire
	public float fireRate = 1f;
	private float fireCountdown = 0f;


	[Header("Unity Setup Fields")]
	public string enemyTag = "Enemy";

	public Transform partToRotate;
	public float turnSpeed = 10f;

	public GameObject bulletPrefab;
	public Transform firePoint;

	
	// Use this for initialization
	void Start () {
	// To update Two times In a Sec.Second field is for when to start.
		InvokeRepeating("UpdateTarget",0f,0.5f);
	}

	void UpdateTarget()
	{
	// Creating a array for enemies 
		GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
	// Creating a temporary variable to store shortest distance. Mathf.Infinity is used to tell if we,
	// don't have any information the distance between us and enemy is infinite.
		float shortestDistance = Mathf.Infinity;
	//To store the nearest enemy we found so far.
		GameObject nearestEnemy = null;
	//Looping through our enemies array.
		foreach (GameObject enemy in enemies)
		{
		// To find distance between our object and enemy.
			float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

			if (distanceToEnemy < shortestDistance)
			{
				shortestDistance = distanceToEnemy;
				nearestEnemy = enemy;
			}
		}

		if (nearestEnemy != null && shortestDistance <= range)
		{
			//We found our closest target.
			target = nearestEnemy.transform;
		}
		//To lose the connection of our enemy if it goes out of range.
		else
		{
			target = null;
		}
	}
	
	// Update is called once per frame
	void Update () {
	//To make sure we aren't doing any thing if there is no target. 
		if(target==null)
			return;
	//***To Rotate our object......Target Lock
		//To get a arrow(eg: a vector or a point) which will point in the direction of our enemy.
		Vector3 dir = target.position - transform.position;
		//Quaternion is unity's way of dealing with rotation.To get how should we need to  rotate our self,
		//to look at direction.
		Quaternion lookRotation = Quaternion.LookRotation(dir);
		//Convert our Quaternion to EulerAngle.
		//Old script*Vector3 rotation = lookRotation.eulerAngles;
		//To make it smooth with Lerp
		Vector3 rotation = Quaternion.Lerp(partToRotate.rotation,lookRotation,Time.deltaTime*turnSpeed).eulerAngles;

		//To rotate our object in only one axis that is y.
		partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);

	//***To Shoot
		if (fireCountdown <= 0f)
		{
			Shoot();
			fireCountdown = 1f / fireRate;
		}

		fireCountdown -= Time.deltaTime;
	}

	void Shoot()
	{
		GameObject bulletGo= (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
		Bullet bullet = bulletGo.GetComponent<Bullet>();

		if (bullet != null)
		{
			bullet.Seek(target);
		}
	}

	// To Display range. To always show range use OnDrawGizmos()
	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position,range);
	}
}
